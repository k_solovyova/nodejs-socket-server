var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var redis = require("redis");
var redisClient = redis.createClient();
var async = require("async");
var bluebird = require("bluebird");


bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const EXPIRE_TIME = 10800; //defaulf - 3 hours

var rooms = new Map();
var disconnected = new Map();



app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


http.listen(port, function () {
  console.log('Server listening at port %d', port);
});


redisClient.on("error", function (err) {
    console.log("Error: " + err);
});



io.on('connection', function(socket){

  console.log('New connection: ' + socket.id);



  socket.on('disconnect', function(){
    console.log('user ' + socket.username + ' disconnected');
    disconnected.set(socket.username, socket.room);
  });



  socket.on('init', function(user_id, trip_id){
  	socket.username = user_id;
  
  	if(disconnected.has(socket.username)){
  	  socket.join(disconnected.get(socket.username), function(){
  		  console.log('User ' + socket.username + ' is back');
  	  });
  	} else {
  	  socket.room = socket.username + trip_id;
  	}

  	console.log('A user ' + socket.username + ' connected');
  });



  socket.on('create room', function(room){
    socket.room = room;

    var roomUsers = [socket.username];
    rooms.set(room, roomUsers);

    socket.join(room, function(){
      console.log('The new room ' + room + ' is created by user ' + socket.username);
    });
  });



  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);
  });



  socket.on('room message', function(msg, room){
    console.log('message: ' + msg + 'room: ' + room);
    io.to(room).emit('room message', msg, room);
  });



  socket.on('subscribe', function(room){
  	socket.room = room;
    var roomUsers = rooms.get(room);
    roomUsers.push(socket.username);
    rooms.set(room, roomUsers);

  	console.log('A new user ' + socket.username + ' has joined the room: ' + room);
  	socket.join(room, function(){
  		//TODO send notification to all users
  		io.to(room, 'A new user ' + socket.username + ' has joined the room'); // broadcast to everyone in the room
  	});
  });



  socket.on('unsubscribe', function(room){
  	console.log('A user ' + socket.username + ' has left the room: ' + room);
  	socket.leave(room, function(){
  		//TODO send notification to all users
      var roomUsers = rooms.get(room);
      var index = roomUsers.indexOf(socket.username);

      if (index > -1) {
        roomUsers.splice(index, 1);
        rooms.set(room, roomUsers);
      } else {
        //TODO throw exception user not found
      }

  		io.to(room, 'A user ' + socket.username + ' has left the room');
  	});
  });



  socket.on('store points', function(room, data){
    var trip_id = data["trip_id"];
    var user_id = socket.username;
    var ltd = data["ltd"];
    var lng = data["lng"];
    var name = data["name"];

    var key = trip_id + user_id;
    var value = {trip_id:trip_id, user_id:user_id, ltd:ltd, lng:lng, name:name};
    value = JSON.stringify(value);

    redisClient.set(key, value, 'EX', EXPIRE_TIME);
  });


  socket.on('get points', function(room){
    var roomUsers = rooms.get(room);
    var userPoints = []; //values - cache data

    var keys = roomUsers; 

    async.each(keys, function(key, callback) {
      redisClient.get(room+key, function(err, value) {
        userPoints[key] = value;
        callback(err);
      });
    }, function() {
      // when callback is finished
      console.log("callback: "+JSON.stringify(userPoints));
    });


    // for (var i = 0; i < roomUsers.length; i++) {
    //   var key = room + roomUsers[i];
    //   var value;


    //   redisClient.get(key, function (err, reply) {
    //     value = JSON.stringify(reply);
    //     userPoints.push(value);
    //     callback(value, this.userPoints);
    //   }.bind({userPoints: userPoints}));
    // }

    // function callback(val, points){
    //   console.log("global: "+val);
    //   points.push(val);
    // }

    console.log(JSON.stringify(userPoints));
    return userPoints;
  });

});


function getValue(key){
    var value = redisClient.getAsync(key).then(function(reply) {
        return reply;
    });

    return Promise.all([value]);
}


function getRedisValue(key){
  redisClient.get(key, function (err, reply) {
    return reply;
  });
}
    